
function Leif_starter(resources)
{
	Leif_starter.resources = resources;
}
Leif_starter.prototype = {
	init: function()
	{
		this.game = new Phaser.Game(800, 600, Phaser.CANVAS, 'leif_starter', { preload: this.preload, create: this.create, update: this.update, render: 
		this.render,parent:this },null,null,false);
	},

	preload: function()
	{
		
		this.game.scale.maxWidth = 800;
    	this.game.scale.maxHeight = 600;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        //pages
		this.game.load.image('page_1',Leif_starter.resources.page_1);
        this.game.load.image('page_2',Leif_starter.resources.page_2);
        this.game.load.image('page_3',Leif_starter.resources.page_3);
		this.game.load.image('page_4',Leif_starter.resources.page_4);
        //buttons
        //static
        this.game.load.image('button_1_static',Leif_starter.resources.button_1_static);
        this.game.load.image('button_2_static',Leif_starter.resources.button_2_static);
        this.game.load.image('button_3_static',Leif_starter.resources.button_3_static);
		this.game.load.image('button_4_static',Leif_starter.resources.button_4_static);
        //down
        this.game.load.image('button_1_down',Leif_starter.resources.button_1_down);
        this.game.load.image('button_2_down',Leif_starter.resources.button_2_down);
        this.game.load.image('button_3_down',Leif_starter.resources.button_3_down);
		this.game.load.image('button_4_down',Leif_starter.resources.button_4_down);
        
        
		
		this.game.created = false;
    	
    	
    	
  
    	this.game.stage.backgroundColor = '#ffffff';
    	//0f2747
    	
	},

	create: function(evt)
	{
		
		if(this.game.created === false)
		{
			
			this.parent.laptop = this.game.make.sprite(0,0,'item_1');
			this.parent.laptop.anchor.set(0.5,0.5);
            this.parent.game.stage.backgroundColor = '#ffffff';
			this.game.created  = true;
	    	this.parent.buildAnimation();
	    }
	},
    
	buildAnimation: function()
	{
		var style = Leif_starter.resources.textStyle_1;
        this.item_1= this.game.add.sprite(this.game.world.centerX-250,this.game.world.centerY-28,'item_1');
        this.item_1.anchor.set(0.5,0.5);
        this.item_1.scale.setTo(.6,.6);
        //
        this.item_2= this.game.add.sprite(this.game.world.centerX,this.game.world.centerY-28,'item_2');
        this.item_2.anchor.set(0.5,0.5);
        this.item_2.scale.setTo(.6,.6);
        //
        this.item_3= this.game.add.sprite(this.game.world.centerX+250,this.game.world.centerY-28,'item_3');
        this.item_3.anchor.set(0.5,0.5);
        this.item_3.scale.setTo(.6,.6);
        //
        var text_1 = this.game.add.text(this.game.world.centerX-260,this.game.world.centerY+230, Leif_starter.resources.text_1, style);
        text_1.anchor.set(0.5,0.5);
        //
        var text_2 = this.game.add.text(this.game.world.centerX+5,this.game.world.centerY+230, Leif_starter.resources.text_2, style);
        text_2.anchor.set(0.5,0.5);
        //
        var text_3 = this.game.add.text(this.game.world.centerX+250,this.game.world.centerY+230, Leif_starter.resources.text_3, style);
        text_3.anchor.set(0.5,0.5);
        
        //animations
        
        this.item_1_An = this.game.add.tween(this.item_1).to( { alpha:1,y:this.item_1.y},500,Phaser.Easing.Quadratic.Out);
        this.item_1.y-=50;
        this.item_1.alpha = 0;
        //
        this.item_2_An = this.game.add.tween(this.item_2).to( { alpha:1,y:this.item_2.y},500,Phaser.Easing.Quadratic.Out);
        this.item_2.y-=50;
        this.item_2.alpha = 0;
        //
        //
        this.item_3_An = this.game.add.tween(this.item_3).to( { alpha:1,y:this.item_3.y},500,Phaser.Easing.Quadratic.Out);
        this.item_3.y-=50;
        this.item_3.alpha = 0;
        
        
        this.text_1_An = this.game.add.tween(text_1).to( { alpha:1,y:text_1.y},500,Phaser.Easing.Quadratic.Out);
        text_1.y+=50;
        text_1.alpha = 0;
        //
        this.text_2_An = this.game.add.tween(text_2).to( { alpha:1,y:text_2.y},500,Phaser.Easing.Quadratic.Out);
        text_2.y+=50;
        text_2.alpha = 0;
        //
        this.text_3_An = this.game.add.tween(text_3).to( { alpha:1,y:text_3.y},500,Phaser.Easing.Quadratic.Out);
        text_3.y+=50;
        text_3.alpha = 0;
        
        
        
        
        //start animation
        
        
        this.item_1_An.chain(this.text_1_An,this.item_2_An,this.text_2_An,this.item_3_An,this.text_3_An);
        this.item_1_An.start();
		
    },
	inview: function()
	{
		
		
	},

	animate: function()
	{
		//console.log("animate")
	},

	update: function()
	{

	},
	render: function()
	{
		//this.game.debug.inputInfo(32, 32);
	}

}
//course/en/animations/nielsen_audience/
var _resources= 
{
    "textStyle_1":{"font":"17px freight-sans-pro","fill": "#000000","wordWrap": false,"wordWrapWidth":200,"align": "center","lineSpacing": -10 },
    "page_1":"/leif_starter/page_1.png",
    "page_2":"/leif_starter/page_2.png",
    "page_3":"/leif_starter/page_3.png",
    "page_4":"/leif_starter/page_4.png",
    "button_1_static":"/leif_starter/button_1_static.png",
    
}
var start = new Leif_starter(_resources).init();


